//
//  JKDispatchQueue.m
//  ProximityBrowser
//
//  Created by Joris Kluivers on 4/30/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <mach/mach_time.h>

#import "JKDispatchQueue.h"

static int JKQueueStackKey  = 1;
static int JKIsCurrentKey = 1;
static int JKIsCurrentValue = 1;

@interface JKDispatchQueue()
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic) JKDeadlockBehavior _deadlockBehavior;
@property (copy) NSString *_label;
@property BOOL concurrent;
@property (strong) NSMutableDictionary *timers;
@end

@implementation JKDispatchQueue

+ (instancetype) dispatchQueueWithGCDQueue:(dispatch_queue_t)gcdQueue behavior:(JKDeadlockBehavior)behavior
{
    JKDispatchQueue *newQueue = [[self alloc] init];
    newQueue.queue = gcdQueue;
    newQueue._deadlockBehavior = behavior;
    dispatch_queue_set_specific(gcdQueue, &JKIsCurrentKey, &JKIsCurrentValue, NULL);
    return newQueue;
}

+ (instancetype) mainDispatchQueue
{
    static dispatch_once_t pred = 0;
    static JKDispatchQueue *mainDispatchQueue = nil;
    dispatch_once(&pred, ^{
        mainDispatchQueue = [self dispatchQueueWithGCDQueue:dispatch_get_main_queue() behavior:JKDeadlockBehaviorExecute];
        const char *label = dispatch_queue_get_label(mainDispatchQueue.queue);
        if (label == NULL)
          label = "unlabeled";
        mainDispatchQueue._label = [NSString stringWithUTF8String:label];
    });
    return mainDispatchQueue;
}

+ (instancetype)globalDispatchQueue
{
    static dispatch_once_t pred = 0;
    static JKDispatchQueue *globalDispatchQueue = nil;
    dispatch_once(&pred, ^
                  {
                      globalDispatchQueue = [self dispatchQueueWithGCDQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) behavior:JKDeadlockBehaviorBlock];
                      const char *label = dispatch_queue_get_label(globalDispatchQueue.queue);
                      if (label == NULL)
                          label = "unlabeled";
                      globalDispatchQueue._label = [NSString stringWithUTF8String:label];
                      globalDispatchQueue.concurrent = YES;
                  });
    return globalDispatchQueue;
}

- (BOOL)scheduleTimerWithName:(NSString *)name timeInterval:(NSTimeInterval)delay behavior:(JKTimerBehavior)behavior block:(JKDispatchBlock)block
{
    __block BOOL success = YES;
    [self dispatchSynchronously:^{
        success = [self _scheduleTimerWithName:name timeInterval:delay behavior:behavior block:block];
    }];
    return success;
}

#pragma mark - Timers

- (NSTimeInterval)_now
{
    static mach_timebase_info_data_t info;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ mach_timebase_info(&info); });
    
    NSTimeInterval t = mach_absolute_time();
    t *= info.numer;
    t /= info.denom;
    return t;
}

- (BOOL)_scheduleTimerWithName:(NSString *)name timeInterval:(NSTimeInterval)delay behavior:(JKTimerBehavior)behavior block:(JKDispatchBlock)block
{
    if (!self.timers)
        self.timers = [NSMutableDictionary dictionary];
    
    NSDictionary *timerInfo = self.timers[name];
    NSNumber *dateValue = timerInfo[@"DateValue"];
    NSValue *timerValue = timerInfo[@"TimerValue"];
    if (!timerValue)
        dateValue = nil;
    
    // get the underlying dispatch timer
    dispatch_source_t dispatchTimer = NULL;
    if (timerValue)
        dispatchTimer = (dispatch_source_t)[timerValue pointerValue];
    else
        dispatchTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, self.queue);
    if (!dispatchTimer)
        return NO;
    
    // adjust firing time only if needed: for coalesce behavior, only take into account earlier-than-currently-set firing
    NSUInteger fireTime = [dateValue doubleValue];
    NSUInteger newFireTime = [self _now] + delay;
    if (dateValue == nil || behavior == JKTimerBehaviorDelay || newFireTime < fireTime)
    {
        dispatch_source_set_timer(dispatchTimer, dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), 0, 0);
    }
    
    // set the new event handler
    if (self.concurrent)
        dispatch_source_set_event_handler(dispatchTimer, ^
                                          {
                                              block();
                                              [self _cancelTimerWithName:name];
                                          });
    else
        dispatch_source_set_event_handler(dispatchTimer, ^
                                          {
                                              NSAssert(dispatch_get_specific(&JKQueueStackKey) == NULL, @"There should be no queue stack set before execution of a block dispatched asynchronously by timer '%@' with queue: %@", name, self);
                                              NSMutableArray *queueStack = [NSMutableArray arrayWithObject:self];
                                              dispatch_queue_set_specific(self.queue, &JKQueueStackKey, (__bridge void *)queueStack, NULL);
                                              block();
                                              NSAssert([queueStack lastObject] == self, @"The queue stack set after execution of a block should have the parent queue as the last object: %@\n Iinstead, it has the following stack: %@", self, queueStack);
                                              [queueStack removeLastObject];
                                              NSAssert([queueStack count] == 0, @"The queue stack should be empty after execution of a block dispatched asynchronously by timer '%@' with queue: %@", name, self);
                                              dispatch_queue_set_specific(self.queue, &JKQueueStackKey, NULL, NULL);
                                              [self _cancelTimerWithName:name];
                                          });
    
    // new timers need to be retained and activated
    if (!timerValue)
    {
        timerValue = [NSValue valueWithPointer:(__bridge_retained const void *)dispatchTimer];
        dispatch_resume(dispatchTimer);
    }
    
    // update timer info
    self.timers[name] = @{ @"DateValue" : @(newFireTime), @"TimerValue" : timerValue };
    
    return YES;
}

- (void) _cancelTimerWithName:(NSString *)name
{
    NSValue *timerValue = self.timers[name][@"TimerValue"];
    if (!timerValue)
        return;
    
    // because we are using NSValue, we need to do the memory management ourselves
    dispatch_source_t dispatchTimer = (__bridge_transfer dispatch_source_t)[timerValue pointerValue];
    dispatch_source_cancel(dispatchTimer);
    [self.timers removeObjectForKey:name];
}

#pragma mark - Accessors

- (NSString *) label
{
    return self._label;
}

- (JKDeadlockBehavior) deadlockBehavior
{
    return self._deadlockBehavior;
}

- (BOOL) isInCurrentQueueStack
{
    NSArray *queueStack = (__bridge NSArray *)(dispatch_get_specific(&JKQueueStackKey));
    return [queueStack containsObject:self];
}

- (void)dispatchSynchronously:(JKDispatchBlock)block
{
    JKDeadlockBehavior behavior = self.deadlockBehavior;
    
    // dispatch_sync will only deadlock if that's the desired behavior
    if (behavior == JKDeadlockBehaviorBlock || ![self isInCurrentQueueStack])
    {
        // prepare the new stack before we are inside the queue, so it can be set on the queue
        NSMutableArray *queueStack = (__bridge NSMutableArray *)(dispatch_get_specific(&JKQueueStackKey));
        BOOL newStack = NO;
        if (!queueStack)
        {
            queueStack = [NSMutableArray array];
            newStack = YES;
        }
        
        // dispatch_queue_set_specific should be serialized within the queue, so it's consistent from one block execution to the next
        dispatch_sync(self.queue, ^{
            if (!self.concurrent)
              [queueStack addObject:self];
            dispatch_queue_set_specific(self.queue, &JKQueueStackKey, (__bridge void *)queueStack, NULL);
            block();
            //NSAssert([queueStack lastObject] == self, @"The queue stack set after execution of a block should have the parent queue as the last object: %@\n Iinstead, it has the following stack: %@", self, queueStack);
            if (!self.concurrent)
              [queueStack removeLastObject];
            NSAssert(!newStack || [queueStack count] == 0, @"The queue stack should be empty after execution of a block dispatched synchronously that was started without a queue stack yet: %@", self);
            dispatch_queue_set_specific(self.queue, &JKQueueStackKey, NULL, NULL);
        });
    }
    
    else
    {
        if (behavior == JKDeadlockBehaviorExecute)
            block();
        else if (behavior == JKDeadlockBehaviorLog)
            NSLog(@"Synchronous dispatch can not be executed on the queue with label '%@' because it is already part of the current dispatch queue hierarchy", self.label);
        else if (behavior == JKDeadlockBehaviorAssert)
            NSAssert(0, @"Synchronous dispatch can not be executed on the queue with label '%@' because it is already part of the current dispatch queue hierarchy", self.label);
    }
}

@end
