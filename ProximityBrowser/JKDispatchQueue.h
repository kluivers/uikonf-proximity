//
//  JKDispatchQueue.h
//  ProximityBrowser
//
//  Created by Joris Kluivers on 4/30/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^JKDispatchBlock)(void);

typedef NS_ENUM(NSInteger, JKTimerBehavior)
{
    JKTimerBehaviorCoalesce,
    JKTimerBehaviorDelay
};

typedef NS_ENUM(NSInteger, JKDeadlockBehavior)
{
    JKDeadlockBehaviorExecute,
    JKDeadlockBehaviorSkip,
    JKDeadlockBehaviorLog,
    JKDeadlockBehaviorAssert,
    JKDeadlockBehaviorBlock
};

@interface JKDispatchQueue : NSObject

+ (instancetype) mainDispatchQueue;
+ (instancetype) globalDispatchQueue;

- (BOOL) scheduleTimerWithName:(NSString *)name timeInterval:(NSTimeInterval)delay behavior:(JKTimerBehavior)behavior block:(JKDispatchBlock)block;

@end
