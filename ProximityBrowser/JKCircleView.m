//
//  JKLightsView.m
//  ProximityBrowser
//
//  Created by Joris Kluivers on 4/28/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import "JKCircleView.h"

@implementation JKCircleView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self jkInitView];
    }
    
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self jkInitView];
    }
    return self;
}

- (void) jkInitView
{
    _circleColor = [NSColor grayColor];
}

- (void) setCircleColor:(NSColor *)circleColor
{
    if ([_circleColor isEqualTo:circleColor]) {
        return;
    }
    
    _circleColor = circleColor;
    
    [self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor whiteColor] setFill];
    [NSBezierPath fillRect:self.bounds];
    
    NSRect insetRect = NSInsetRect(self.bounds, 40.0, 40.0);
    
    CGFloat diameter = fminf(NSWidth(insetRect), NSHeight(insetRect));
    
    NSRect circleRect = NSMakeRect((NSWidth(self.bounds) - diameter) / 2.0f, (NSHeight(self.bounds) - diameter) / 2.0f, diameter, diameter);
    
    [self.circleColor setFill];
    NSBezierPath *circle = [NSBezierPath bezierPathWithOvalInRect:circleRect];
    [circle fill];
    
}

@end
