//
//  JKAppDelegate.m
//  ProximityBrowser
//
//  Created by Joris Kluivers on 4/28/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <IOBluetooth/IOBluetooth.h>

#import "JKAppDelegate.h"

#import "JKCircleView.h"
#import "JKDispatchQueue.h"

@interface CBUUID (StringExtraction)
- (NSString *) jk_UUIDString;
@end

static NSInteger AVERAGE_VALUE_COUNT = 15;

typedef enum {
    JKStateIdle,
    JKStateBrowsing,
    JKStateDetected,
    JKStateNearby
} JKBLEState;

@interface JKAppDelegate () <CBCentralManagerDelegate>
@property(nonatomic, strong) CBCentralManager *manager;
@property(nonatomic, strong) NSNumber *averageRSSI;
@end

@implementation JKAppDelegate {
    JKBLEState _state;
    
    NSMutableArray *_receivedValues;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    _receivedValues = [NSMutableArray array];
    
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    // Insert code here to initialize your application
    [self setState:JKStateIdle];
}

- (IBAction) toggleBrowsing:(id)sender
{
    if (_state == JKStateIdle) {
        [self setState:JKStateBrowsing];
        
        NSLog(@"Start scan!");
        
        [self.manager scanForPeripheralsWithServices:nil options:@{
            CBCentralManagerScanOptionAllowDuplicatesKey: @YES
        }];
    } else {
        [self setState:JKStateIdle];
        
        [self.manager stopScan];
    }
}

- (void) setState:(JKBLEState)state
{
    _state = state;
    
    NSColor *color = nil;
    NSString *stateText = nil;
    
    switch (state) {
        default:
        case JKStateIdle:
            color = [NSColor grayColor];
            stateText = NSLocalizedString(@"Idle", nil);
            [self.toggleBrowseButton setTitle:NSLocalizedString(@"Start", nil)];
            break;
        case JKStateBrowsing:
            color = [NSColor colorWithCalibratedRed:213/255.0f green:12/255.0f blue:12/255.0f alpha:1.0];
            stateText = NSLocalizedString(@"No devices detected", nil);
            [self.toggleBrowseButton setTitle:NSLocalizedString(@"Stop", nil)];
            break;
        case JKStateDetected:
            color = [NSColor orangeColor];
            stateText = NSLocalizedString(@"Devices detected nearby", nil);
            break;
        case JKStateNearby:
            color = [NSColor greenColor];
            stateText = NSLocalizedString(@"Devices very close!", nil);
            break;
    }
    
    self.circleView.circleColor = color;
    [self.statusLabel setStringValue:stateText];
}

#pragma mark - Central Manager

- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"State updated!");
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        NSLog(@"Ready!");
    }
}

- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    // to generate random UUID
    // uuidgen | tr -d '\n' | pbcopy
    
    // We're scanning for all services, but filtering out all but the one with the
    // following UUID. On stage I used an iPhone app that published service with this UUID
    // using CBPeripheralManager
    CBUUID *serviceUUID = [CBUUID UUIDWithString:@"FE85EBDF-9587-475B-A7EF-2541D87BB9DB"];
    
    NSArray *services = advertisementData[CBAdvertisementDataServiceUUIDsKey];
    if (![services containsObject:serviceUUID]) {
        return;
    }
    
    [self pushReceivedRSSI:RSSI];
    
    [[JKDispatchQueue globalDispatchQueue] scheduleTimerWithName:@"no_devices_found" timeInterval:0.2 behavior:JKTimerBehaviorDelay block:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setState:JKStateBrowsing];
        });
    }];
}

- (void) pushReceivedRSSI:(NSNumber *)rssi
{
    [_receivedValues addObject:rssi];
    if ([_receivedValues count] > AVERAGE_VALUE_COUNT) {
        [_receivedValues removeObjectAtIndex:0];
    }
    
    NSInteger total = 0;
    for (NSNumber *rssi in _receivedValues) {
        total += [rssi integerValue];
    }
    
    NSInteger average = total / (NSInteger)[_receivedValues count];
    
    self.averageRSSI = @(average);
    
    [self updateDisplay];
}

- (void) updateDisplay
{
    if (ABS([self.averageRSSI integerValue]) < 40) {
        [self setState:JKStateNearby];
    } else {
        [self setState:JKStateDetected];
    }
}

@end

@implementation CBUUID (StringExtraction)

- (NSString *) jk_UUIDString;
{
    NSData *data = [self data];
    
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *outputString = [NSMutableString stringWithCapacity:16];
    
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++)
    {
        switch (currentByteIndex)
        {
            case 3:
            case 5:
            case 7:
            case 9:[outputString appendFormat:@"%02x-", uuidBytes[currentByteIndex]]; break;
            default:[outputString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
        }
        
    }
    
    return outputString;
}

@end
