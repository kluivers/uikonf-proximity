//
//  JKAppDelegate.h
//  ProximityBrowser
//
//  Created by Joris Kluivers on 4/28/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class JKCircleView;

@interface JKAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (nonatomic, weak) IBOutlet NSTextField *statusLabel;
@property (nonatomic, weak) IBOutlet NSButton *toggleBrowseButton;
@property (nonatomic, weak) IBOutlet JKCircleView *circleView;

- (IBAction) toggleBrowsing:(id)sender;

@end
