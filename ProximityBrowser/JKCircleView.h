//
//  JKLightsView.h
//  ProximityBrowser
//
//  Created by Joris Kluivers on 4/28/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface JKCircleView : NSView

@property(nonatomic, strong) NSColor *circleColor;

@end
